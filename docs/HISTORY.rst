Changelog
=========

3.2.1 (unreleased)
------------------

- Nothing changed yet.


3.2.0 (2014-10-27)
------------------

- Initial release (knockout.js 3.2.0)

